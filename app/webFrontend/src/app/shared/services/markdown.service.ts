import {Injectable} from '@angular/core';
import * as MarkdownIt from 'markdown-it';
import * as markdownItEmoji from 'markdown-it-emoji';
import twemoji from 'twemoji';
import * as MarkdownItDeflist from 'markdown-it-deflist';
import * as MarkdownItContainer from 'markdown-it-container';
import * as MarkdownItMark from 'markdown-it-mark';
import * as MarkdownItAbbr from 'markdown-it-abbr';
import * as hljs from 'highlight.js';

@Injectable()
export class MarkdownService {
  markdown: any;


  constructor() {
    this.markdown = new MarkdownIt({
      highlight: function (str, lang) {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return hljs.highlight(lang, str).value;
          } catch (__) {
          }
        }

        return ''; // use external default escaping
      }
    });

    // load plugins
    this.markdown.use(markdownItEmoji);
    this.markdown.use(MarkdownItDeflist);

    // register warning, info, error, success as custom containers
    this.markdown.use(MarkdownItContainer, 'warning');
    this.markdown.use(MarkdownItContainer, 'info');
    this.markdown.use(MarkdownItContainer, 'error');
    this.markdown.use(MarkdownItContainer, 'success');
    this.markdown.use(MarkdownItContainer, 'learning-objectives');
    this.markdown.use(MarkdownItContainer, 'hints');
    this.markdown.use(MarkdownItContainer, 'assignment');
    this.markdown.use(MarkdownItContainer, 'question');
    this.markdown.use(MarkdownItContainer, 'example');
    this.markdown.use(MarkdownItContainer, 'todo');

    this.markdown.use(MarkdownItMark);

    this.markdown.use(MarkdownItAbbr);


    this.markdown.renderer.rules.emoji = function (token, idx) {
      return twemoji.parse(token[idx].content);
    };
  }

  render(text: string): string {
    return this.markdown.render(text);
  }
}
