import {Contributor} from './contributor.model';

// This file contains all contributors
export class ContributorsList {
  public static getAllContributors(): Contributor[] {
    return [
      // Leader
      new Contributor('Prem Chandra', 'Singh', 'File Upload, Dashboard, Layouts', '1801130', 'utetrapp'),
      new Contributor('Shivam', 'Kumar', "User Management, Authentication", '1801166', 'd89'),
      new Contributor('Yash', 'Agarwal', 'Forum, Search, DB', '1801199', 'd89')
    ];
 }
}
