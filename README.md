 # IIITG-LMS


## Development

The codebase is written entirely in [TypeScript](https://www.typescriptlang.org/).

The API is based on Nodejs together with Express, routing controllers and PostgreSQL.


## Local Deployment 

### Running locally

For running locally you first have to install Node.js and PostgreSQL on your machine.

**It is important to only use versions of Node.js and PostgreSQL that are currently 
supported by the project.**

For running multiple versions of Node.js [nvm](https://github.com/creationix/nvm)
is a very good option.

Install Dependencies:

    cd api
    npm install
    cd ../app/webFrontend
    npm install
    cd ..


## Running the API server

Then you can start up the API server:

    cd api
    npm run start


## Running the web frontend

    cd app/webFrontend
    npm run start


Both commands will automatically watch for filesystem changes and recompile the code
once you change it.

After successfully starting, the web frontend will be available at [http://localhost:4200](http://localhost:4200).

The API will be available at [http://localhost:3030](http://localhost:3030). To avoid [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) the API will also be proxied by
the development server and be available at 
[http://localhost:4200/api](http://localhost:4200/api).


## Configuration

The API can be configured using the following environment variables.

| Variable | Description | Example value |
| --- | --- | --- |
| `BASEURL` | The URL you are hosting the web frontend at. | `http://localhost:4200` |
| `DB_HOST` | The host your PostgreSQL instance is running on | `localhost` |
| `DB_PORT` | The port your PostgreSQL instance is listening on | `58201` |
| `PORT` | The port the API instance should listen on | `80` |
| `SECRET` | Secret key for JWT signing and encryption | - |


### Commands

#### API & web frontend

  - __Running__: `npm run start`
  - __Linting__: `npm run lint`
  - __Testing__: `npm run test`

#### API

  - __Loading fixtures__ (sample data): `npm run load:fixtures`
  - __[Debugging](https://nodejs.org/en/docs/inspector/)__: `npm run start:inspect`

#### Web frontend

  - __Running__: `npm run start`
  - __End to end tests__: `npm run e2e`


## LICENSING

See the License for the specific language governing permissions and limitations under the License.
